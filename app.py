import sys
from PyQt5.QtWidgets import QApplication
from model.conexao_bd import Databases
from controller.principal import Principal

if __name__ == '__main__':

    app = QApplication(sys.argv)
    bd = Databases().abrir_bd_sqlite()
    
    principal = Principal(bd = bd)
    principal.showNormal()
    sys.exit(app.exec_())
