import os
from PyQt5.QtSql import QSqlDatabase, QSqlQuery


class Databases:
    def __init__(self):
        self.sqlite = None

    def abrir_bd_sqlite(self):
        caminho = os.getcwd()
        bancoDeDados = QSqlDatabase.addDatabase("QSQLITE")
        bancoDeDados.setDatabaseName(os.path.join(caminho,"storage.db"))
        bd_ok = bancoDeDados.open()

        if not bd_ok:
            print('Problemas com o banco de dados!')
        return bancoDeDados
