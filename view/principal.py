# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'view/principal.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 565)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName("gridLayout")
        self.tableTasks = QtWidgets.QTableView(self.centralwidget)
        self.tableTasks.setObjectName("tableTasks")
        self.gridLayout.addWidget(self.tableTasks, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.toolBar = QtWidgets.QToolBar(MainWindow)
        self.toolBar.setIconSize(QtCore.QSize(32, 32))
        self.toolBar.setObjectName("toolBar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
        self.actionConfigurar = QtWidgets.QAction(MainWindow)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("resource/config.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionConfigurar.setIcon(icon)
        self.actionConfigurar.setObjectName("actionConfigurar")
        self.actionAddTarefa = QtWidgets.QAction(MainWindow)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("resource/add.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionAddTarefa.setIcon(icon1)
        self.actionAddTarefa.setObjectName("actionAddTarefa")
        self.actionRemoveTarefa = QtWidgets.QAction(MainWindow)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("resource/remove.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionRemoveTarefa.setIcon(icon2)
        self.actionRemoveTarefa.setObjectName("actionRemoveTarefa")
        self.actionExecutarTarefas = QtWidgets.QAction(MainWindow)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("resource/exec-tasks.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionExecutarTarefas.setIcon(icon3)
        self.actionExecutarTarefas.setObjectName("actionExecutarTarefas")
        self.actionEditarTarefa = QtWidgets.QAction(MainWindow)
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap("resource/edit.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionEditarTarefa.setIcon(icon4)
        self.actionEditarTarefa.setObjectName("actionEditarTarefa")
        self.toolBar.addAction(self.actionExecutarTarefas)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionAddTarefa)
        self.toolBar.addAction(self.actionEditarTarefa)
        self.toolBar.addAction(self.actionRemoveTarefa)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionConfigurar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Task Runner"))
        self.toolBar.setWindowTitle(_translate("MainWindow", "toolBar"))
        self.actionConfigurar.setText(_translate("MainWindow", "Configurar"))
        self.actionConfigurar.setToolTip(_translate("MainWindow", "Configurar"))
        self.actionAddTarefa.setText(_translate("MainWindow", "Adicionar Tarefa"))
        self.actionAddTarefa.setToolTip(_translate("MainWindow", "AdicionarTarefa"))
        self.actionRemoveTarefa.setText(_translate("MainWindow", "Remover Tarefa"))
        self.actionRemoveTarefa.setToolTip(_translate("MainWindow", "Remover Tarefa"))
        self.actionExecutarTarefas.setText(_translate("MainWindow", "Executar Tarefas"))
        self.actionExecutarTarefas.setToolTip(_translate("MainWindow", "Executar Tarefas"))
        self.actionEditarTarefa.setText(_translate("MainWindow", "Editar Tarefa"))
        self.actionEditarTarefa.setToolTip(_translate("MainWindow", "Editar Tarefa"))
