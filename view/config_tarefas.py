# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'view/config_tarefas.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ConfigTarefas(object):
    def setupUi(self, ConfigTarefas):
        ConfigTarefas.setObjectName("ConfigTarefas")
        ConfigTarefas.resize(653, 521)
        self.gridLayout_2 = QtWidgets.QGridLayout(ConfigTarefas)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.buttonSalvar = QtWidgets.QPushButton(ConfigTarefas)
        self.buttonSalvar.setObjectName("buttonSalvar")
        self.gridLayout_2.addWidget(self.buttonSalvar, 2, 1, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem, 2, 0, 1, 1)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label_4 = QtWidgets.QLabel(ConfigTarefas)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 6, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(ConfigTarefas)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 4, 0, 1, 1)
        self.textExito = QtWidgets.QTextEdit(ConfigTarefas)
        self.textExito.setObjectName("textExito")
        self.gridLayout.addWidget(self.textExito, 5, 0, 1, 1)
        self.label = QtWidgets.QLabel(ConfigTarefas)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.editNome = QtWidgets.QLineEdit(ConfigTarefas)
        self.editNome.setObjectName("editNome")
        self.gridLayout.addWidget(self.editNome, 1, 0, 1, 2)
        self.label_2 = QtWidgets.QLabel(ConfigTarefas)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)
        self.label_5 = QtWidgets.QLabel(ConfigTarefas)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 8, 0, 1, 1)
        self.textScript = QtWidgets.QTextEdit(ConfigTarefas)
        self.textScript.setObjectName("textScript")
        self.gridLayout.addWidget(self.textScript, 3, 0, 1, 1)
        self.textErro = QtWidgets.QTextEdit(ConfigTarefas)
        self.textErro.setObjectName("textErro")
        self.gridLayout.addWidget(self.textErro, 7, 0, 1, 1)
        self.textDescricao = QtWidgets.QTextEdit(ConfigTarefas)
        self.textDescricao.setObjectName("textDescricao")
        self.gridLayout.addWidget(self.textDescricao, 9, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 2)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem1, 1, 0, 1, 1)

        self.retranslateUi(ConfigTarefas)
        QtCore.QMetaObject.connectSlotsByName(ConfigTarefas)

    def retranslateUi(self, ConfigTarefas):
        _translate = QtCore.QCoreApplication.translate
        ConfigTarefas.setWindowTitle(_translate("ConfigTarefas", "Configuração de contexto"))
        self.buttonSalvar.setText(_translate("ConfigTarefas", "Salvar"))
        self.label_4.setText(_translate("ConfigTarefas", "Mensagem em caso de Erro:"))
        self.label_3.setText(_translate("ConfigTarefas", "Mensagem em caso de Êxito:"))
        self.label.setText(_translate("ConfigTarefas", "Nome:"))
        self.label_2.setText(_translate("ConfigTarefas", "Script:"))
        self.label_5.setText(_translate("ConfigTarefas", "Descrição:"))
