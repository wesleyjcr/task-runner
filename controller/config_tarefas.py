from view.config_tarefas import Ui_ConfigTarefas
from PyQt5.QtWidgets import QDialog, QMessageBox
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtSql import QSqlQuery

class ConfigTarefas(QDialog, Ui_ConfigTarefas): 
    def __init__(self, parent = None, bd = None):
        super(ConfigTarefas, self).__init__(parent)
        self.setupUi(self)
        self.bd = bd
        self.tarefa = {}

    @pyqtSlot()
    def on_buttonSalvar_clicked(self):
        self.captura_campos()
        self.validar_campos()
        self.salvar_campos()

    def captura_campos(self):
        self.tarefa['nome'] = self.editNome.text()
        self.tarefa['script'] = self.textScript.toPlainText()
        self.tarefa['msg_exito'] = self.textExito.toPlainText()
        self.tarefa['msg_erro'] = self.textErro.toPlainText()
        self.tarefa['descricao'] = self.textDescricao.toPlainText()

    def validar_campos(self):
        for chave, valor in self.tarefa.items():
            if len(valor) == 0:
                self.tarefa[chave] = None
    
    def salvar_campos(self):
        query = QSqlQuery(self.bd)
        query.prepare('INSERT INTO TAREFA (NOME, SCRIPT, MSG_EXITO, MSG_ERRO, DESCRICAO) VALUES (?,?,?,?,?);')
        query.addBindValue(self.tarefa['nome'])
        query.addBindValue(self.tarefa['script'])
        query.addBindValue(self.tarefa['msg_exito'])
        query.addBindValue(self.tarefa['msg_erro'])
        query.addBindValue(self.tarefa['descricao'])
        if query.exec_():
            QMessageBox.information(None, "Inserido com sucesso", "Os dados foram inseridos com sucesso!")   
            self.close()
        else:
            QMessageBox.warning(None, "Database Error",query.lastError().text())   

        