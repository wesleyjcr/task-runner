from view.principal import Ui_MainWindow
from controller.config_tarefas import ConfigTarefas
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtSql import QSqlQuery, QSqlTableModel

class Principal(QMainWindow, Ui_MainWindow): 
    def __init__(self, parent = None, bd = None):
        super(Principal, self).__init__(parent)
        self.setupUi(self)
        self.bd = bd
        self.model = QSqlTableModel(self, self.bd)
        self.atualizaTabela()
        
    #-Menu Superior
    @pyqtSlot()
    def on_actionExecutarTarefas_triggered(self):
        tarefas = ConfigTarefas(self, bd = self.bd)
        tarefas.show()
    
    @pyqtSlot()
    def on_actionAddTarefa_triggered(self):
        tarefas = ConfigTarefas(self, bd = self.bd)
        tarefas.show()
    
    @pyqtSlot()
    def on_actionEditarTarefa_triggered(self):
        tarefas = ConfigTarefas(self, bd = self.bd)
        tarefas.show()
    
    @pyqtSlot()
    def on_actionRemoveTarefa_triggered(self):
        tarefas = ConfigTarefas(self, bd = self.bd)
        tarefas.show()
    
    @pyqtSlot()
    def on_actionConfigurar_triggered(self):
        tarefas = ConfigTarefas(self, bd = self.bd)
        tarefas.show()
    
    def atualizaTabela(self):
        consulta = 'SELECT * FROM TAREFA'
        self.model.setQuery(QSqlQuery(consulta, self.bd))
        self.model.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.tableTasks.setModel(self.model)
        self.tableTasks.resizeColumnsToContents()
